CC = gcc
src = keeplog
OBJECTS1 = $(addsuffix .o, $(CFILES))
CFILES1 = $(wildcard *.c)
CFILES = $(patsubst %.c, %, $(CFILES1))


all: $(src)

$(src): $(OBJECTS1)
	$(CC) $(OBJECTS1) -o $(src)

$(OBJECTS1): %.o: %.c
	$(CC) -c $(CFILES1)
	$(CC) -MM $(CFILES1) > $*.d

-include $(addsuffix .d, $(CFILES))

.PHONY: clean
clean:
	@rm *.o *.d $(src)
